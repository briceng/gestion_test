package com.projet.tp.models.entitie;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Utilisateur implements Serializable {


    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String nom;
    private String prenom;
    private int age;
    private String matricule;
    private String adresse;
    private String ville;
    @Enumerated(EnumType.STRING)
    private Sexe sexe;
    private String email;
    private String password;
    @Enumerated(EnumType.STRING)
    private Role role;
    private String poste;
    private String telephone;
    private String address;
    @Enumerated(EnumType.STRING)
    private Etat etat;

    @OneToMany(mappedBy="agent")
    private List<Affectation> listeAffectations;

    @OneToMany(mappedBy="vehicule")
    private List<Depense> depenseList;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getMatricule() {
        return matricule;
    }

    public void setMatricule(String matricule) {
        this.matricule = matricule;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public String getVille() {
        return ville;
    }

    public void setVille(String ville) {
        this.ville = ville;
    }

    public Sexe getSexe() {
        return sexe;
    }

    public void setSexe(Sexe sexe) {
        this.sexe = sexe;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public String getPoste() {
        return poste;
    }

    public void setPoste(String poste) {
        this.poste = poste;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Etat getEtat() {
        return etat;
    }

    public void setEtat(Etat etat) {
        this.etat = etat;
    }

    /**
    @OneToMany(mappedBy="agent")
    private List<Affectation> Listaffectation;
*/


    public Utilisateur(String email, String password, Role role,Sexe sexe) {
        this.sexe = sexe;
        this.email = email;
        this.password = password;
        this.role = role;
    }

    public enum Sexe {
        MASCULIN, FEMININ
    }
    public enum Role {
        Agent, Admin
    }
    public enum Etat {
        Active, Inactive
    }


}


