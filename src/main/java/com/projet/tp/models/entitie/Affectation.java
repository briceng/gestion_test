package com.projet.tp.models.entitie;


import lombok.*;

import javax.persistence.*;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Affectation {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @ManyToOne
    @JoinColumn(name="agent")
    private Utilisateur agent;
    @ManyToOne
    @JoinColumn(name="vehicule")
    private Vehicule vehicule;
    private String date;


}
