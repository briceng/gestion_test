package com.projet.tp.models.entitie;


import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;


@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Vehicule implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String immatriculation;

    private String date_acquisition;
    private String marque;

    @Enumerated(EnumType.STRING)
    private Energie energie;
    private double kilometrage;
    @Enumerated(EnumType.STRING)
    private Etat etat;
    private String description;

    @ManyToOne
    @JoinColumn(name="categorieid")
    private Categorie categorie;

    @OneToMany(mappedBy="vehicule")
    private List<Affectation> listeAffectation;

    @OneToMany(mappedBy="utilisateur")
    private List<Depense> depenseList;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getImmatriculation() {
        return immatriculation;
    }

    public void setImmatriculation(String immatriculation) {
        this.immatriculation = immatriculation;
    }

    public String getDate_acquisition() {
        return date_acquisition;
    }

    public void setDate_acquisition(String date_acquisition) {
        this.date_acquisition = date_acquisition;
    }

    public String getMarque() {
        return marque;
    }

    public void setMarque(String marque) {
        this.marque = marque;
    }

    public Energie getEnergie() {
        return energie;
    }

    public void setEnergie(Energie energie) {
        this.energie = energie;
    }

    public double getKilometrage() {
        return kilometrage;
    }

    public void setKilometrage(double kilometrage) {
        this.kilometrage = kilometrage;
    }

    public Etat getEtat() {
        return etat;
    }

    public void setEtat(Etat etat) {
        this.etat = etat;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Categorie getCategorie() {
        return categorie;
    }

    public void setCategorie(Categorie categorie) {
        this.categorie = categorie;
    }

    @Override
    public String toString() {
        return "Vehicule{" +
                "id=" + id +
                ", immatriculation='" + immatriculation + '\'' +
                ", date_acquisition='" + date_acquisition + '\'' +
                ", marque='" + marque + '\'' +
                ", energie=" + energie +
                ", kilometrage=" + kilometrage +
                ", etat=" + etat +
                ", description='" + description + '\'' +
                ", categorie=" + categorie +
                ", listeAffectation=" + listeAffectation +
                ", depenseList=" + depenseList +
                '}';
    }

    /**
    @OneToMany(mappedBy="vehicule")
    private List<Affectation> Listaffectation;
*/




    public enum Etat {
        Occuper, Libre
    }

    public enum Energie {
        Essence, Gazoil,Electrique, Petrole, Carburant
    }
}
