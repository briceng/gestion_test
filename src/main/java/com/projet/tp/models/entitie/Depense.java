package com.projet.tp.models.entitie;


import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;



@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Depense implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private Double montant;

    private String date_depense;

    @Enumerated(EnumType.STRING)
    private TypeDepense typeDepense;

    private double kilometrage;
    @ManyToOne
    @JoinColumn(name="agentId")
    private Utilisateur utilisateur;

    @ManyToOne
    @JoinColumn(name="vehiculeId")
    private Vehicule vehicule;

    private String description;



    public enum TypeDepense {
        Consommation, Maintenance
    }

}
