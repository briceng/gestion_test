package com.projet.tp.models.entitie;


import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Categorie implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String libele;
    private String description;

    @OneToMany(mappedBy="categorie")
    private List<Vehicule> Listmateriel;




}
