package com.projet.tp.presentation.controller;


import com.projet.tp.models.dto.AffectationDto;
import com.projet.tp.models.dto.DepenseDto;
import com.projet.tp.models.entitie.Affectation;
import com.projet.tp.models.entitie.Utilisateur;
import com.projet.tp.services.serviceImpl.AffectationService;
import com.projet.tp.services.serviceImpl.DepenseService;
import com.projet.tp.services.serviceImpl.UtilisateurService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.security.Principal;

@Controller
public class DepenseController {

    @Autowired
    private DepenseService depenseService;

    @Autowired
    private UtilisateurService utilisateurService;

    @Autowired
    private AffectationService affectationService;
    private DepenseDto depenseD;

    @GetMapping("/lister_depense")
    String listeDepense(Model model){

        model.addAttribute("depenses",depenseService.getListDepense());
        return "depense/lister_depense";
    }

    @GetMapping("/enregistrer_depense")
    String ajouter_depense(){

        return "depense/ajouter_depense";
    }


    @GetMapping("/depense_agent")
    String listeDepense_Agent(Principal principal, Model model){

       Utilisateur agent = utilisateurService.findUserByMail(principal.getName());

        System.out.println("====================================********************************"+agent);

        model.addAttribute("depenses", depenseService.getListDepenseByAgent(agent.getId()));

        return "agent/liste_depense";
    }

    @GetMapping("/save_depense")
    String agentSaveDepense(Principal principal,Model model){
        Utilisateur agent = utilisateurService.findUserByMail(principal.getName());

        Affectation affectationDto = affectationService.findByAgent(agent.getId());

        System.out.println("after-------===============-----------"+affectationDto);
        DepenseDto depenseDto = new DepenseDto();
        depenseDto.setVehicule(affectationDto.getVehicule());
        System.out.println("before---------------"+affectationDto.getVehicule());
        depenseDto.setUtilisateur(agent);
        depenseD=depenseDto;
        model.addAttribute("depenseDto", depenseDto);

        return "agent/save_depense";
    }

    @PostMapping("/store_depense")
    String store_depense(@ModelAttribute DepenseDto depenseDto){


        depenseDto.setUtilisateur(depenseD.getUtilisateur());
        depenseDto.setVehicule(depenseD.getVehicule());
        //System.out.println(depenseDto);
        System.out.println(depenseDto.getVehicule());
        depenseService.saveDepense(depenseDto);

        return "redirect:/depense_agent";
    }

}
