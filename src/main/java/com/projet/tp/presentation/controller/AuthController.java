package com.projet.tp.presentation.controller;


import com.projet.tp.models.entitie.Utilisateur;
import com.projet.tp.repositories.UtilisateurRepository;
import com.projet.tp.services.serviceImpl.UtilisateurService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.security.Principal;

@Controller
public class AuthController {

    @Autowired
    private BCryptPasswordEncoder encoder;
    @Autowired
    private UtilisateurRepository utilisateurRepository;
    @Autowired
    private UtilisateurService utilisateurService;
    @GetMapping("/seed")
    public void saveU(){
        Utilisateur user= new Utilisateur("admin@gmail.com",encoder.encode("admin"), Utilisateur.Role.Admin, Utilisateur.Sexe.MASCULIN);
        utilisateurRepository.save(user);
    }

    @GetMapping("/login")
    String login(){
        return "auth/login";
    }

    @GetMapping("/forgot_password")
    String forgotPassword(){

        return "auth/forgotPassword";
    }

    @GetMapping("/reset_password")
    String reset_password(){

        return "auth/reset_password";
    }




    @GetMapping("/logout")
    public String logout() {

        return "auth/login";
    }

    @GetMapping("/profile")
    public String profile(Principal principal, Model model) {
        Utilisateur agent = utilisateurService.findUserByMail(principal.getName());
        model.addAttribute("user",agent);

        return "setting_profile/setting_profile";
    }






}
