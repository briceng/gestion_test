package com.projet.tp.repositories;


import com.projet.tp.models.entitie.Affectation;
import com.projet.tp.models.entitie.Depense;
import com.projet.tp.models.entitie.Utilisateur;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AffectationRepository extends JpaRepository<Affectation, Long> {


    boolean existsById(Long id);

    Affectation findByAgent(Utilisateur agent);
    @Query(value ="select * from  affectation  a  where (d.agentId =:agentId and d.vehiculeId=:vehiculeId ) ",nativeQuery =true)
    Affectation findId(@Param("agentId") Long agentId);




}
